from telethon.sync import TelegramClient, events
import json, sqlite3
from PIL import Image
from datetime import datetime

with open('config.json', 'r') as f:
    config = json.load(f)

conn = sqlite3.connect('database.db')
db = conn.cursor()

bot = TelegramClient('bot', config['api_id'], config['api_hash']).start(bot_token=config['bot_token'])

@bot.on(events.NewMessage)
async def handler(event):
    tid = event.from_id
    path = config['download_path']
    link = config['download_link']
    if event.message.gif:
        filename = '{}.{}'.format(int(datetime.timestamp(event.message.date)), 'mp4')
        await event.message.download_media(''.join([path, filename]))
        await event.respond(''.join(['Ecco il link:\n', link, filename]))
    elif event.message.sticker:
        temp_filename = '{}.{}'.format(int(datetime.timestamp(event.message.date)), 'webp')
        filename = '.'.join([temp_filename.split('.')[0], 'png'])
        cache = ''.join([config['cache_path'], temp_filename])
        await event.message.download_media(cache)
        img = Image.open(cache)
        img.save(''.join([path, filename]), 'PNG')
        await event.respond(''.join(['Ecco il link:\n', link, filename]))
    else:
        print("ok")

bot.start()
bot.run_until_disconnected()
